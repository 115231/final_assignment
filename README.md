This project analyses factors affecting flight delays in the U.S.

Users need to first install Jupyter Notebook before running the code. 

Users will be able to identify whether a flight will delay based on given conditions.

A compressed zip file must first be downloaded to unzip. The zip file is available via this link.

Link:https://ucstaff-my.sharepoint.com/personal/ibrahim_radwan_canberra_edu_au/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fibrahim%5Fradwan%5Fcanberra%5Fedu%5Fau%2FDocuments%2Fteaching%5Fjunction%2F2021%2Fs2%2Fdsts%2Ffinal%5Fproject%2Fdata%5Fcompressed&ga=1